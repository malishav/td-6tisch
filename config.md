## Test configuration

### Node under Test (NUT)

In the context of 6TiSCH, and according to RFC8180, a Node Under Test is a low-power wireless node equipped with a IEEE802.15.4-compliant radio, and implementing at least:

- the IEEE802.15.4e TSCH MAC protocol
- the 6LoWPAN adaptation layer
- the RPL routing protocol

In the scope of this Test Description, a NUT also implements:

- ICMPv6 echo request/reply (ping)
- IPv6 over Low-Power Wireless Personal Area Network (6LoWPAN) Routing Header (RFC8138)
- IEEE 802.15.4 Information Element for the IETF (RFC8137)
- draft-ietf-6tisch-minimal-security-06
- The Constrained Application Protocol (CoAP) (RFC7252)
- draft-ietf-core-object-security-13

### System under Test (SUT)

The System Under Test (SUT) is composed of a number of Nodes Under Test (NUTs), possibly implemented by different vendors.
Each test case specifies the scenario to be used.
To address different functional areas and groups of tests, the following SUT scenario have been defined.

#### Single hop

6TiSCH single-hop topology includes a DAGroot and a 6TiSCH Node.
In order to verify the correct formatting of the frames exchanged between the DR and the 6N, a packet sniffer is also needed.


                   +----------------+
                   |                |
                   | Packet Sniffer |
                   |                |
                   +----------------+


      +----------+                    +-------------+
      |          | +----------------> |             |
      | DAG root |                    | 6TiSCH Node |
      |          | <----------------+ |             |
      +----------+                    +-------------+


#### Multi-hop

The multi-hop scenario includes 1 DR and more than one 6Ns, forming a linear topology.
This topology is used for some of the tests in this TD.



                                  +----------------+
                                  |                |
                                  | Packet Sniffer |
                                  |                |
                                  +----------------+


	+----------+         +-------------+                     +-------------+
	|          | +-----> |             | +----->     +-----> |             |
	| DAG root |         | 6TiSCH Node |         ...         | 6TiSCH Node |
	|          | <-----+ |             | <-----+     <-----+ |             |
	+----------+         +-------------+                     +-------------+

## IEEE802.15.4 Default Parameters

All the tests are performed using the following setting.

### Address length

ALL IEEE802.15.4 addresses will be long (64-bit). The exception is broadcast address:

    Broadcast Address: 0xffff.

### Frame version

ALL IEEE802.15.4 frames will be of version 2 (b10).

### PAN ID compression and sequence number

All IEEE802.15.4 frames will contain the following field:

- a source address,
- a destination address,
- a sequence number,
- a destination PANID (no source PANID).

### Payload termination IE

The IE payload list termination will NOT be included in the EB.

### Slotframe length.

Unless otherwise stated, the slotframe length is set to 11 slots.

### RPL Operation Mode

There are two modes for a RPL Instrance to choose for maintaning Downward
routes: Storing and Non-Storing modes. We use the Non-Storing mode during the
tests.

## Secure Join Default Parameters

Secure join (SECJOIN) tests are performed using the following common parameters and configuration settings.

### Configuration

JRC is co-located with DR.

For some tests, JP acts maliciously and alters or replay the requests, in order to test the error behavior of JRC.
How this is implemented is implementation-specific.

### Link-layer Requirements

#### EB Period

For all SECJOIN tests, the DR sends EBs periodically, with a fast rate (equal to 10 sec, according to IEEE802.15.4std), so that the Pledge does not need to send KAs for keeping synchronization.

#### Channel Hopping

All frames are sent on a single frequency -- channel hopping is disabled.

#### Layer 2 Security

The Layer 2 SEC option is enabled on DR and Pledge.

The value of key K1 and K2 will be set to:

    0x11111111111111111111111111111111

Moreover, Key Index (advertised in the auxiliary security header of the 802.15.4 frame), will be used for K1 and K2, to enable nodes to look up the right key before decrypting.

The keys K1 and K2 are unknown to the Pledge, forcing the Pledge to go through the join procedure to obtain them.

### Common Security Context

JRC and Pledge share an OSCORE security context established out-of-band.

The OSCORE Master Secret is set to:

    0xDEADBEEFCAFEDEADBEEFCAFEDEADBEEF

Algorithm is set to AES-CCM-16-64-128.

The rest of the OSCORE context is derived as per draft-ietf-6tisch-minimal-security-04.

### Resources Exposed by JRC

JRC runs a CoAP (RFC7252) server exposing /j resource, that is OSCORE protected with GET as the only allowed method.

### CoAP Option Numbers

Since Object-Security CoAP option number defined in draft-ietf-core-object-security-07 has not yet been defined by IANA, we use the value 21.
Similarly, for Stateless-Proxy option defined in draft-ietf-6tisch-minimal-security-04, we use the value 40.

