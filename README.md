# Test Description of Constrained Join Protocol

Version: 2018-07-05 15:11:26.649413

This test description (TD) facilitates automated, remote testing of [Constrained Join Protocol](https://datatracker.ietf.org/doc/draft-ietf-6tisch-minimal-security/) as enabled by the [SPOTS](http://spots.ac.me) and [F-Interop](http://www.f-interop.eu) projects.

The TD follows the structure used for previous 6TiSCH interop events.
Tests have been redesigned and updated to leverage the dissection tools developed by the SPOTS project.
Tests from this TD will be continuously synced with the corresponding tests of the official 6TiSCH TD.

University of Montenegro reserves the right to update the TD as tests are implemented in the F-Interop 6TiSCH testing tool. 
An up to date version of the TD can be found at: [https://bitbucket.org/malishav/td-6tisch](https://bitbucket.org/malishav/td-6tisch).

This repository describes:

- The configurations used during test sessions, including the relevant parameter values of the different layers (IEEE802.15.4, TSCH, 6TiSCH, RPL).

- The test descriptions, describing the scenarios to follow to perform the tests.

## Table of contents:

- Overview of Constrained Join Protocol
- Introduction to Interoperability Test Process
- Definition & Abbreviations
- Configuration
- Conventions
- Testing with F-Interop platform
- Test Cases
