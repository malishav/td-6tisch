## Definitions

### Synchronicity & Time Correction

2 nodes are considered synchronized if and only if they can maintain a time drift lower than 40 [ppm](https://en.wikipedia.org/wiki/Parts-per_notation) from each other.

For example if two nodes do not re-synchronize for 10s, their desynchronization must be below 400 us:

    10 s * 40 ppm = 400 us

More information about this topic can be found [here](https://openwsn.atlassian.net/wiki/display/OW/Synchronization).

### Joining a network

A node (pledge) successfully joined a network if and only if it performed the following steps successfully:

- It got time-synchronized
- It completed the security handshake and received a join response
- It received routing information

## Abreviations

| Abbreviation  | Meaning                                |
| ------------- | -------------                          |
| 6N            | 6TiSCH Node                            |
| EB            | Enhanced Beacon frame                  |
| IE            | Information Element                    |
| JP            | Join Proxy                             |
| JRC           | Join Registrar/Coordinator             |
| KA            | Keep-Alive message                     |
| NUT           | Node Under Test                        |
| RPI           | RPL Information Option                 |
| SUT           | System Under Test                      |
| TD            | Test Description                       |
| PSK           | Pre-Shared Key, also known as join key |



## Equipment Type:

- DAGroot (DR):	A DAGroot is a 6TiSCH Node acting as root of the routing DAG in the 6TiSCH network topology.

- 6TiSCH Node (6N):	A 6TiSCH Node is any node within a 6TiSCH network other than the DAGroot.
	It can act as parent and/or child node within the DAG.
	It can communicate with its children and its parent using the 6TiSCH minimal schedule, or any other TSCH schedule.
	In the test description, the term is used to refer to a non-DAGroot node.

- Join Registrar/Coordinator (JRC): central entity responsible for authentication and authorization of joining nodes (pledges).

- Pledge: A 6N, before having completed the join process.

- Join Proxy (JP): A 6N, playing the role of a stateless relay that provides connectivity between the pledge and the JRC during the join process.
