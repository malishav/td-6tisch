## Overview of Constrained Join Protocol

The goal of the Constrained Join Protocol specification is to allow a freshly unboxed 6TiSCH device to become a meaningful network node in a secure manner.
The device that attempts to join is referred to as the Pledge.
The entity that enforces the authorization decisions of the network administrator is referred to as the Join Registrar/Coordinator (JRC).
A third entity is called a Join Proxy (JP): JP is a network node within radio range of the Pledge.
JP simply forwards the messages between the Pledge and the JRC.
Pledge and JRC share a symmetric key that allows mutual authentication, confidentiality and integrity of the protocol execution.
This key is referred to as the Pre-Shared Key (PSK) or simply a join key.

Constrained Join Protocol leverages the OSCORE mechanism to provide confidentiality, integrity and replay protection.
Pledge and JRC use the PSK as an OSCORE Master Secret, based on which other keying material is derived and used to protect the protocol execution.
Constrained Join Protocol consists in two messages: Join Request and Join Response.
Join request is issued by the Pledge and destined to the JRC.
The goal of join request is to notify JRC of Pledge's existence, to authenticate the Pledge to the JRC and for the Pledge to request the link-layer keying material needed to join the network.
Join response is issued by the JRC and destined to the Pledge.
The goal of join response is to authenticate the JRC to the Pledge, and to return the keying material requested.
Sensitive information in both join request and join response messages is encrypted through OSCORE.

In this test description, we cover conformance aspects of the specification as well as error handling that is security sensitive.

For more details on Constrained Join Protocol, the reader is referred to the latest [technical specification](https://datatracker.ietf.org/doc/draft-ietf-6tisch-minimal-security/).

