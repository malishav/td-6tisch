Encoding.default_external = Encoding::UTF_8
require 'yaml'
require 'haml'
require 'erb'

task :default => [
  "abbreviations.htm",
  "config.htm",
  "conventions.htm",
  "description.htm",
  "f-interop.htm",
  "golden_device.htm",
  "intro.htm",
  "README.htm",
  "references.htm",
  "tests_d_secjoin.pdf",

  "plugtest",
]

HAML_SPEC = <<'HAML_SPEC'
%html
  %head
    %meta(charset="utf-8")
    %title 6lo
    :css
      table {
        border-collapse: collapse;
        font-family: Arial, Sans-Serif;
        margin-bottom: 2em;
      }
      table.big {
        font-size: 0.8rem;
      }
      table td {
        min-width: 30px;
        padding: 3px 0 3px 9px;
      }
      table th {
        padding: 5px 0 5px 0;
      }
      @media print {
        table {page-break-inside: avoid;}
      }
  %body
    - $input.sort.each do |k, v|
      - next if k == "strings"
      - next if v["dis"]
      - big = v.delete("big")
      %table{border: 1, width: "100%", class: ("big" if big)}
        %tr
          %th(colspan=4) Interoperability Test Description
        %tr
          %th(width="15%") Identifier:
          %td(colspan=3)
            = k
        %tr
          %th Objective:
          %td(colspan=3)
            = v.delete("obj")
        %tr
          %th Configuration:
          %td(colspan=3)
            = contify(v.delete("cfg"))
        %tr
          %th References:
          %td(colspan=3)
            = contify(v.delete("ref"))
        %tr
          %th Pre-test conditions:
          %td(colspan=3)
            = contify(v.delete("pre"))
        %tr
          %th Test Sequence:
          %td(width="10 %") Step
          %td(width="15 %") Type
          %td Description
        - v.delete("seq").each_with_index do |step, i|
          - fail unless step.keys.size == 1
          - k = step.keys.first
          %tr
            %td
            %td
              = i
            %td
              = {"s" => "Stimulus", "c" => "Check", "v" => "Verify", "f" => "Feature"}[k]
            %td
              - s = step[k]
              = contify(s)
        - [["not", "Notes:"], ["iss", "Issues:"]].each do |sect|
          - if s = v.delete(sect[0])
            %th
              = sect[1]
            %td(colspan=3)
              = contify(s)
        - fail v unless v.empty?
HAML_SPEC

def contify(x)
  case x
  when Array
    if x.size == 1
      html_escape(x.first)
    else
      x.map {|el| "<li>#{contify(el)}</li>\n"}.join
    end
  when String
    html_escape(x)
  else
    raise x.inspect
  end
end

rule ".html" => [".yml"] do |t|
  $input = YAML.load(File.read(t.source))
  engine = Haml::Engine.new(HAML_SPEC)
  File.write(t.name, engine.render)
end

rule ".htm" => [".md"] do |t|
  sh "pandoc --from markdown --to html #{t.source} -o #{t.name}"
  sh "xvfb-run -a -- wkhtmltopdf --print-media-type #{t.name} #{t.name}.pdf"
end

rule ".pdf" => [".html"] do |t|
  # Bug in debian version of wkhtmltopdf
  # https://unix.stackexchange.com/questions/192642/wkhtmltopdf-qxcbconnection-could-not-connect-to-display
  # https://stackoverflow.com/questions/16726227/xvfb-failed-start-error
  sh "xvfb-run -a -- wkhtmltopdf --print-media-type #{t.source} #{t.name}"
end

task :plugtest do
  # Put the test in a specific order
  sh "python bump_version.py"
  sh "pdftk cover_page.pdf README.htm.pdf description.htm.pdf intro.htm.pdf abbreviations.htm.pdf conventions.htm.pdf config.htm.pdf f-interop.htm.pdf tests_d_secjoin.pdf cat output SPOTS_D1.2.pdf"
end

task :clean do
  sh "rm -f *.pdf *.html *.htm"
end
