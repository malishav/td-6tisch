# Conventions

The tests are designed to be executable remotely on the F-Interop platform and 6TiSCH Testing Tool, as well as manually during, for example, a plugtest event.

For manual execution, it is necessary to provide following tools: 

- **Sniffer**: An IEEE802.15.4 compliant sniffer and the relevant tools to be able to analyze packets exchanges over the air.

- **Dissector**: A computer program capable of interpreting the frames captured by the packet sniffer, and verify the correct formatting of the different headers inside that frame.

In case of execution with the F-Interop platform, the dissection and automated test analysis is performed in the Cloud, while the user does need to integrate a sniffer with the F-Interop agent component.
How the sniffer is integrated with F-Interop agent is out-of-scope of this description, but is tackled by F-Interop partners.

## Test Description Naming Convention

All the tests described in this document keep the naming convention used by the official 6TiSCH TD, in order to facilitate reuse.
All the tests described in this document correspond to the following category in the official 6TiSCH TD:

- Security JOIN (SECJOIN): Test Join security features

Following the official 6TiSCH TD, this TD uses a Test ID following the naming convention:
TD_6TiSCH_SEJOIN_X, where X is a unique identifier of a test within category SECJOIN.
